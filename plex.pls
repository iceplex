#!/usr/bin/perl -w
#
# iceplex
#
# Copyright (c) 2003 Ivan Kohler
# All rights reserved.
# This program is free software; you can redistribute it and/or modify it under
# the same terms as Perl itself.

use strict;
use subs qw(suidsetup pick_server);
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use Digest::MD5 qw(md5_hex);
use DBI;
use Net::SSH qw(ssh);

suidsetup();

# pull in configuration
use vars qw($dsn $user $pass $mp3path $port);
use vars qw($override_query $fileno_query);
use vars qw(%extension2type);
require "/etc/iceplex.conf";

#detect .pls or .m3u filetype
$0 =~ /\.(pls|m3u)$/i or die "must be named with .pls or .m3u extension";
my $extension = lc($1);

#connect to database
my $dbh = DBI->connect($dsn, $user, $pass) or die $DBI::errstr;

#pick a server
my $server = pick_server();

#get customer & user from client browser
my $cgi = new CGI;
$cgi->param('customer') =~ /^(\d{1,10})$/ or die 'illegal customer numer';
my $customer = sprintf("%6d", $1); #!! how many digits in customer #?
$cgi->param('user') =~ /^(\d{1,10})$/ or die 'illegal user numer';
my $user = sprintf("%4d", $1); #!! how many digits in user #?

#get file number
my $fileno = '';
if ( $override_query ) {
  my $sth = $dbh->prepare( $override_query ) or die $dbh->errstr;
  $sth->execute($customer, $user) or die $sth->errstr;
  my $row = $sth->fetchrow_arrayref;
  $fileno = $row->[0] if $row;
}
if ( !length($fileno) && $fileno_query ) {
  my $sth = $dbh->prepare( $fileno_query ) or die $dbh->errstr;
  $sth->execute($customer, $user) or die $sth->errstr;
  my $row = $sth->fetchrow_arrayref;
  die "No files for user $user of customer $customer" unless $row;
  $fileno = $row->[0];
}
$fileno = '000' unless length($fileno); #default

#genarate filename
my $filename = "$mp3path/$customer-$user-$fileno.mp3";

#disconnect from database
$dbh->disconnect;

#generate a mountpoint
md5_hex($cgi->remote_host. $$. time. int(rand(4294967296))) =~ /^(\w+)$/;
my $mountpoint = $1;

#signal encoder to start streaming to mountpoint & wait for confirmation
warn "ssh $server yashout $filename $mountpoint $port\n";
ssh($server, 'yashout', $filename, $mountpoint, $port );

#send file back to client browser

print $cgi->header( -type => $extension2type{$extension} );
print "[playlist]\nNumberOfEntries=1\nFile1=" if $extension eq 'pls';
print "http://$server:$port/$mountpoint\n";

###
# subroutiens
###

sub suidsetup {
  $ENV{'PATH'} ='/usr/local/bin:/usr/bin:/usr/ucb:/bin';
  $ENV{'SHELL'} = '/bin/sh';
  $ENV{'IFS'} = " \t\n";
  $ENV{'CDPATH'} = '';
  $ENV{'ENV'} = '';
  $ENV{'BASH_ENV'} = '';

  $< = $>; #correct uid for spawned ssh process
}

sub pick_server {
  my $sth = $dbh->prepare(
    "SELECT servername FROM iceplex_servers ".
    "WHERE status = 'online' ".
    "ORDER BY listeners ASC LIMIT 1"
  ) or die $dbh->errstr;
  $sth->execute() or die $sth->errstr;
  my $row = $sth->fetchrow_arrayref;
  die "No servers online!" unless $row;
  $row->[0];
}

