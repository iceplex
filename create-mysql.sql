create table iceplex_servers (
  servernum int primary key auto_increment,
  servername varchar(255),
  listeners int,
  status varchar(255),
);

