iceplex

Copyright (c) 2003 Ivan Kohler
All rights reserved.
This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

ivan-iceplex@420.am

iceplex is a system for multiplexing on-demand broadcasts to multiple
backend icecast servers.

To use:

Database configuration:

  - Create the "iceplex_servers" table in your database
    (see create-mysql.sql or create-Pg.sql)
  - Populate the "iceplex_servers" table with your icecast server names.

On the central multiplexing server:

  - Copy the example iceplex.conf to /etc/iceplex.conf and modify the settings
    as appropriate:
    - Database location/user/pass
    - Path to mp3 file storage on the icecast servers
    - Icecast port number
    - Optional database query for file number override
    - Normal database query for filen number
  - Run iceplexd and configure your init scripts to start it upon boot.
  - Create an "iceplex" user.
  - Run "ssh-keygen -t dsa" as the "iceplex" user to genarate SSH keys.
    Use a blank passphrase.  (Press return when prompted for a passphrase)
  - Install Net::SSH from CPAN or <http://search.cpan.org/author/IVAN/Net-SSH/>
  - Install DBI from CPAN or <http://search.cpan.org/author/TIMB/DBI/>
  - Install the DBD for your database
    - MySQL: DBD::mysql from CPAN or
      <http://search.cpan.org/author/JWIED/DBD-mysql/>
    - PostgreSQL: DBD::Pg from CPAN or
      <http://search.cpan.org/author/DWHEELER/DBD-Pg/>
  - Install plex.pls in /cgi-bin/ on the central multiplexing server or main
    webserver.  This file can be installed as or linked to as "plex.m3u" if
    desired.  chown this file to the "iceplex", chgrp this file to the
    user Apache runs as (www-data or httpd), and chown this file to 4750

On each icecast server:

  - Create an "iceplex" user.
  - Copy "/home/iceplex/.ssh/id_dsa.pub" from the central multiplexing server
    to "/home/iceplex/.ssh/authorized_keys".
  - Verify that the iceplex user on the central multiplexing server can
    ssh to this server without a password.
  - Install libshout C library 1.0.9 and Shout perl interface:
    <http://developer.icecast.org/libshout/>
  - Set the encoder password in yashout
  - Install yashout from this archive in /usr/local/bin

On end-user webpages:

  - Link to (see example.html):
      <http://multi.plexing.server/cgi-bin/plex.pls?customer=XXXXXX;user=YYYY>

Optional, improves stream start latency:

  - Install fsh <http://www.lysator.liu.se/fsh/> on the central multiplexing
    server and each icecast server.
  - Uncomment 
      $Net::SSH:ssh = 'fsh';
    in /etc/iceplex.conf

